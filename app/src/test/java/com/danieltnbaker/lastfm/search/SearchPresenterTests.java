package com.danieltnbaker.lastfm.search;

import com.danieltnbaker.lastfm.R;
import com.danieltnbaker.lastfm.search.mvp.SearchMvp;
import com.danieltnbaker.lastfm.search.mvp.SearchPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.danieltnbaker.lastfm.search.ui.SearchActivity.TRACK;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class SearchPresenterTests {

    @Mock
    private SearchMvp.View view;
    private SearchMvp.Presenter presenter;

    @Before
    public void setup() {
        presenter = new SearchPresenter();
        presenter.setView(view);
    }

    @After
    public void tearDown() {
        presenter.releaseView();
    }

    @Test
    public void testSearchWithValidQueryInitiatesSearch() {
        presenter.onSearchInitiated(TRACK, "query");
        verify(view).loadSearchPage(eq(TRACK), eq("query"));
        verifyNoMoreInteractions(view);
    }

    @Test
    public void testSearchWithNullQueryBlocksSearch() {
        presenter.onSearchInitiated(TRACK, null);
        verify(view).setErrorForSearchInput(eq(R.string.error_search_input));
        verifyNoMoreInteractions(view);
    }

    @Test
    public void testSearchWithEmptyQueryBlocksSearch() {
        presenter.onSearchInitiated(TRACK, "");
        verify(view).setErrorForSearchInput(eq(R.string.error_search_input));
        verifyNoMoreInteractions(view);
    }
}
