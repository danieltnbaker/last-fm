package com.danieltnbaker.lastfm.list;


import com.danieltnbaker.lastfm.utils.TestUtils;
import com.danieltnbaker.lastfm.list.mvp.ListMvp;
import com.danieltnbaker.lastfm.list.mvp.ListPresenter;
import com.danieltnbaker.lastfm.model.Album;
import com.danieltnbaker.lastfm.model.Artist;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.model.Track;
import com.danieltnbaker.lastfm.net.AutoValueGsonFactory;
import com.danieltnbaker.lastfm.net.LastFmRepo;
import com.danieltnbaker.lastfm.net.model.AlbumResultsDataModel;
import com.danieltnbaker.lastfm.net.model.ArtistResultsDataModel;
import com.danieltnbaker.lastfm.net.model.TrackResultsDataModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;

import static com.danieltnbaker.lastfm.search.ui.SearchActivity.ALBUM;
import static com.danieltnbaker.lastfm.search.ui.SearchActivity.ARTIST;
import static com.danieltnbaker.lastfm.search.ui.SearchActivity.TRACK;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ListPresenterTests {

    @Mock
    private ListMvp.View view;
    @Mock
    private LastFmRepo repo;

    private ListMvp.Presenter presenter;
    private TestScheduler testScheduler;

    private Gson gson;

    @Before
    public void setup() {
        gson = new GsonBuilder()
                .registerTypeAdapterFactory(AutoValueGsonFactory.create())
                .create();
        testScheduler = new TestScheduler();
        presenter = new ListPresenter(repo, testScheduler, testScheduler);
        presenter.setView(view);
    }

    @After
    public void tearDown() {
        presenter.releaseView();
    }

    @Test
    public void testLoadingOfAlbumListOnViewReady() {
        Observable<AlbumResultsDataModel> observable = Observable.just(gson.fromJson(TestUtils.ALBUM_JSON, AlbumResultsDataModel.class));
        when(repo.performAlbumSearch("test", 1)).thenReturn(observable);
        presenter.onViewReady(ALBUM, "test");
        verify(view).showProgress();
        verify(repo).performAlbumSearch("test", 1);
        testScheduler.triggerActions();
        verify(view).loadResults(getAlbumList(observable));
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testLoadingOfArtistListOnViewReady() {
        Observable<ArtistResultsDataModel> observable = Observable.just(gson.fromJson(TestUtils.ARTIST_JSON, ArtistResultsDataModel.class));
        when(repo.performArtistSearch("test", 1)).thenReturn(observable);
        presenter.onViewReady(ARTIST, "test");
        verify(view).showProgress();
        verify(repo).performArtistSearch("test", 1);
        testScheduler.triggerActions();
        verify(view).loadResults(getArtistList(observable));
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testLoadingOfTrackListOnViewReady() {
        Observable<TrackResultsDataModel> observable = Observable.just(gson.fromJson(TestUtils.SONG_JSON, TrackResultsDataModel.class));
        when(repo.performTrackSearch("test", 1)).thenReturn(observable);
        presenter.onViewReady(TRACK, "test");
        verify(view).showProgress();
        verify(repo).performTrackSearch("test", 1);
        testScheduler.triggerActions();
        verify(view).loadResults(getTrackList(observable));
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testHandlesNetworkErrorOnViewReady() {
        Observable<AlbumResultsDataModel> observable = Observable.error(new Throwable());
        when(repo.performAlbumSearch("test", 1)).thenReturn(observable);
        presenter.onViewReady(ALBUM, "test");
        verify(view).showProgress();
        verify(repo).performAlbumSearch("test", 1);
        testScheduler.triggerActions();
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoInteractionsWhenViewReleasedOnViewReady() {
        Observable<AlbumResultsDataModel> observable = Observable.just(gson.fromJson(TestUtils.ALBUM_JSON, AlbumResultsDataModel.class));
        when(repo.performAlbumSearch("test", 1)).thenReturn(observable);
        presenter.onViewReady(ALBUM, "test");
        verify(view).showProgress();
        verify(repo).performAlbumSearch("test", 1);
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testLoadingOfJokesOnEndOfPageReached() {
        ((ListPresenter) presenter).setValuesForTest(ALBUM, "test");
        Observable<AlbumResultsDataModel> observable = Observable.just(gson.fromJson(TestUtils.ALBUM_JSON, AlbumResultsDataModel.class));
        when(repo.performAlbumSearch("test", 1)).thenReturn(observable);
        presenter.onEndOfPageReached();
        verify(view).showProgress();
        verify(repo).performAlbumSearch(eq("test"), eq(1));
        testScheduler.triggerActions();
        verify(view).loadResults(getAlbumList(observable));
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testHandlesNetworkErrorOnEndOfPageReached() {
        ((ListPresenter) presenter).setValuesForTest(ALBUM, "test");
        Observable<AlbumResultsDataModel> observable = Observable.error(new Throwable());
        when(repo.performAlbumSearch("test", 1)).thenReturn(observable);
        presenter.onEndOfPageReached();
        verify(view).showProgress();
        verify(repo).performAlbumSearch(eq("test"), eq(1));
        testScheduler.triggerActions();
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoInteractionsWhenViewReleasedOnEndOfPageReached() {
        ((ListPresenter) presenter).setValuesForTest(ALBUM, "test");
        Observable<AlbumResultsDataModel> observable = Observable.just(gson.fromJson(TestUtils.ALBUM_JSON, AlbumResultsDataModel.class));
        when(repo.performAlbumSearch("test", 1)).thenReturn(observable);
        presenter.onEndOfPageReached();
        verify(view).showProgress();
        verify(repo).performAlbumSearch(eq("test"), eq(1));
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testDetailPageLoadedOnItemClicked() {
        Album object = Album.create("", "", "", "");
        presenter.onItemClicked(object);
        verify(view).loadDetailsPage(eq(object));
        verifyNoMoreInteractions(view, repo);
    }

    private List<LastFmObject> getAlbumList(Observable<AlbumResultsDataModel> observable) {
        return observable
                .flatMapIterable(x -> x.getResults().getAlbumMatches().getAlbum())
                .map(Album::create)
                .cast(LastFmObject.class)
                .toList()
                .blockingGet();
    }

    private List<LastFmObject> getArtistList(Observable<ArtistResultsDataModel> observable) {
        return observable
                .flatMapIterable(x -> x.getResults().getArtistMatches().getArtist())
                .map(Artist::create)
                .cast(LastFmObject.class)
                .toList()
                .blockingGet();
    }

    private List<LastFmObject> getTrackList(Observable<TrackResultsDataModel> observable) {
        return observable
                .flatMapIterable(x -> x.getResults().getTrackMatches().getTrack())
                .map(Track::create)
                .cast(LastFmObject.class)
                .toList()
                .blockingGet();
    }
}
