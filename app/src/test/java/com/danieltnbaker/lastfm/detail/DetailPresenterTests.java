package com.danieltnbaker.lastfm.detail;


import com.danieltnbaker.lastfm.detail.mvp.DetailMvp;
import com.danieltnbaker.lastfm.detail.mvp.DetailsPresenter;
import com.danieltnbaker.lastfm.model.Album;
import com.danieltnbaker.lastfm.model.Artist;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.model.Track;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class DetailPresenterTests {

    @Mock
    private DetailMvp.View view;
    private DetailMvp.Presenter presenter;

    @Before
    public void setup() {
        presenter = new DetailsPresenter();
        presenter.setView(view);
    }

    @After
    public void tearDown() {
        presenter.releaseView();
    }

    @Test
    public void testAlbumObjectDisplaysCorrectDetails() {
        LastFmObject object = Album.create("name", "thumbnail", "cover", "artist");
        presenter.onViewReady(object);
        verify(view).loadBasicInformation(eq(object));
        verify(view).loadAlbumDetails((Album) eq(object));
        verifyNoMoreInteractions(view);
    }

    @Test
    public void testArtistObjectDisplaysCorrectDetails() {
        LastFmObject object = Artist.create("name", "thumbnail", "cover", "listeners");
        presenter.onViewReady(object);
        verify(view).loadBasicInformation(eq(object));
        verify(view).loadArtistDetails((Artist) eq(object));
        verifyNoMoreInteractions(view);
    }

    @Test
    public void testTrackObjectDisplaysCorrectDetails() {
        LastFmObject object = Track.create("name", "thumbnail", "cover", "artist", "listeners");
        presenter.onViewReady(object);
        verify(view).loadBasicInformation(eq(object));
        verify(view).loadTrackDetails((Track) eq(object));
        verifyNoMoreInteractions(view);
    }
}
