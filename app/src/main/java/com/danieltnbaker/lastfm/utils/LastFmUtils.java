package com.danieltnbaker.lastfm.utils;


import java.text.NumberFormat;
import java.util.Locale;

public class LastFmUtils {

    public static String formatListeners(String listeners) {
        if (listeners != null && !listeners.isEmpty()) {
            Integer integer = Integer.valueOf(listeners);
            return NumberFormat.getNumberInstance(Locale.getDefault()).format(integer) + " listeners";
        }
        return null;
    }
}
