package com.danieltnbaker.lastfm.search.mvp;


import android.support.annotation.StringRes;

public interface SearchMvp {

    interface Presenter {
        void setView(View view);
        void onSearchInitiated(int searchType, String query);
        void releaseView();
    }

    interface View {
        void loadSearchPage(int searchType, String query);
        void setErrorForSearchInput(@StringRes int errorResId);
    }
}
