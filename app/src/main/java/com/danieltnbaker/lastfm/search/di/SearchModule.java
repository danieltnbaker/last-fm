package com.danieltnbaker.lastfm.search.di;

import com.danieltnbaker.lastfm.search.mvp.SearchMvp;
import com.danieltnbaker.lastfm.search.mvp.SearchPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchModule {

    @Provides @Singleton
    public SearchMvp.Presenter providesSearchPresenter() {
        return new SearchPresenter();
    }
}
