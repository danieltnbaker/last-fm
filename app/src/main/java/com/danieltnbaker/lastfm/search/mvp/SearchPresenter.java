package com.danieltnbaker.lastfm.search.mvp;


import com.danieltnbaker.lastfm.R;
import com.danieltnbaker.lastfm.search.ui.SearchActivity;

public class SearchPresenter implements SearchMvp.Presenter {

    private SearchMvp.View mView;

    @Override
    public void setView(SearchMvp.View view) {
        mView = view;
    }

    @Override
    public void onSearchInitiated(@SearchActivity.SearchType int searchType, final String query) {
        if (query != null && !query.isEmpty()) {
            mView.loadSearchPage(searchType, query);
        } else {
            mView.setErrorForSearchInput(R.string.error_search_input);
        }
    }

    @Override
    public void releaseView() {
        mView = null;
    }
}
