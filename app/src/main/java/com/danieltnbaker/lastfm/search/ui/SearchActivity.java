package com.danieltnbaker.lastfm.search.ui;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.IntDef;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.danieltnbaker.lastfm.R;
import com.danieltnbaker.lastfm.di.DI;
import com.danieltnbaker.lastfm.list.ui.ListActivity;
import com.danieltnbaker.lastfm.search.mvp.SearchMvp;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class SearchActivity extends AppCompatActivity implements SearchMvp.View {

    public static final int TRACK = 0;
    public static final int ARTIST = 1;
    public static final int ALBUM = 2;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TRACK, ARTIST, ALBUM})
    public @interface SearchType {
    }

    @BindView(R.id.search_text_input_layout)
    TextInputLayout textInputLayout;

    @BindView(R.id.search_text_input)
    EditText searchTextInput;

    @BindView(R.id.search_radio_group)
    RadioGroup searchTypeGroup;

    @Inject
    SearchMvp.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        DI.getAppComponent().inject(this);
        mPresenter.setView(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.releaseView();
        super.onDestroy();
    }

    @Override
    public void loadSearchPage(int searchType, String query) {
        startActivity(ListActivity.newIntent(this, searchType,
                searchTextInput.getText().toString()));
    }

    @Override
    public void setErrorForSearchInput(@StringRes int errorResId) {
        textInputLayout.setError(getString(errorResId));
    }

    @OnClick(R.id.button_search)
    public void onSearchButtonClick() {
        mPresenter.onSearchInitiated(
                onRadioButtonChecked(searchTypeGroup.getCheckedRadioButtonId()),
                searchTextInput.getText().toString());
    }

    @OnEditorAction(R.id.search_text_input)
    public boolean onCharacterTextInputChanged(TextView v, int actionId) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            textInputLayout.setError(null);
            mPresenter.onSearchInitiated(
                    onRadioButtonChecked(searchTypeGroup.getCheckedRadioButtonId()),
                    v.getText().toString());
            return true;
        }
        return false;
    }

    private int onRadioButtonChecked(@IdRes int buttonId) {
        switch (buttonId) {
            case R.id.radio_button_song:
                return TRACK;
            case R.id.radio_button_artist:
                return ARTIST;
            case R.id.radio_button_album:
                return ALBUM;
            default:
                return -1;
        }
    }
}
