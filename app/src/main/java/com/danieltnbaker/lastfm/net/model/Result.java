package com.danieltnbaker.lastfm.net.model;


import com.google.gson.annotations.SerializedName;

public abstract class Result {

    @SerializedName("opensearch:Query")
    protected abstract Query getQuery();

    @SerializedName("opensearch:totalResults")
    protected abstract String getTotalResults();

    @SerializedName("opensearch:startIndex")
    protected abstract String getStartIndex();

    @SerializedName("opensearch:itemsPerPage")
    protected abstract String getItemsPerPage();
}
