package com.danieltnbaker.lastfm.net;

import java.util.HashMap;
import java.util.Map;

class QueryMapBuilder {

    private static final String KEY_ALBUM = "album";
    private static final String KEY_ARTIST = "artist";
    private static final String KEY_TRACK = "track";
    private static final String KEY_LIMIT = "limit";
    private static final String KEY_PAGE = "page";

    private final Map<String, Object> queryMap;

    QueryMapBuilder() {
        queryMap = new HashMap<>();
    }

    QueryMapBuilder album(String album) {
        queryMap.put(KEY_ALBUM, album);
        return this;
    }

    QueryMapBuilder artist(String artist) {
        queryMap.put(KEY_ARTIST, artist);
        return this;
    }

    QueryMapBuilder track(String track) {
        queryMap.put(KEY_TRACK, track);
        return this;
    }

    QueryMapBuilder page(int page) {
        queryMap.put(KEY_PAGE, page);
        return this;
    }

    Map<String, Object> build() {
        queryMap.put(KEY_LIMIT, 20);
        if (!queryMap.containsKey(KEY_PAGE)) page(1);
        return queryMap;
    }
}
