package com.danieltnbaker.lastfm.net;


import com.danieltnbaker.lastfm.net.model.AlbumResultsDataModel;
import com.danieltnbaker.lastfm.net.model.ArtistResultsDataModel;
import com.danieltnbaker.lastfm.net.model.TrackResultsDataModel;

import io.reactivex.Observable;

public class LastFmRepo {

    private final LastFmService mService;

    public LastFmRepo(LastFmService service) {
        mService = service;
    }

    public Observable<TrackResultsDataModel> performTrackSearch(String track, int page) {
        return mService.performTrackSearch(new QueryMapBuilder()
                .track(track).page(page).build());
    }

    public Observable<ArtistResultsDataModel> performArtistSearch(String artist, int page) {
        return mService.performArtistSearch(new QueryMapBuilder()
                .artist(artist).page(page).build());
    }

    public Observable<AlbumResultsDataModel> performAlbumSearch(String album, int page) {
        return mService.performAlbumSearch(new QueryMapBuilder()
                .album(album).page(page).build());
    }
}
