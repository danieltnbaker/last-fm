package com.danieltnbaker.lastfm.net.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;


@AutoValue
public abstract class ArtistResultsDataModel {

    public abstract ArtistResult getResults();

    @AutoValue
    public static abstract class ArtistResult extends Result {

        @SerializedName("artistmatches")
        public abstract ArtistMatches getArtistMatches();

        @AutoValue
        public static abstract class ArtistMatches {

            public abstract List<ArtistDataModel> getArtist();

            public static TypeAdapter<ArtistMatches> typeAdapter(Gson gson) {
                return new AutoValue_ArtistResultsDataModel_ArtistResult_ArtistMatches.GsonTypeAdapter(gson);
            }
        }

        public static TypeAdapter<ArtistResult> typeAdapter(Gson gson) {
            return new AutoValue_ArtistResultsDataModel_ArtistResult.GsonTypeAdapter(gson);
        }
    }

    public static TypeAdapter<ArtistResultsDataModel> typeAdapter(Gson gson) {
        return new AutoValue_ArtistResultsDataModel.GsonTypeAdapter(gson);
    }
}

