package com.danieltnbaker.lastfm.net.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@AutoValue
public abstract class AlbumResultsDataModel {

    public abstract AlbumResult getResults();

    @AutoValue
    public static abstract class AlbumResult extends Result {

        @SerializedName("albummatches")
        public abstract AlbumMatches getAlbumMatches();

        @AutoValue
        public static abstract class AlbumMatches {

            public abstract List<AlbumDataModel> getAlbum();

            public static TypeAdapter<AlbumMatches> typeAdapter(Gson gson) {
                return new AutoValue_AlbumResultsDataModel_AlbumResult_AlbumMatches.GsonTypeAdapter(gson);
            }
        }

        public static TypeAdapter<AlbumResult> typeAdapter(Gson gson) {
            return new AutoValue_AlbumResultsDataModel_AlbumResult.GsonTypeAdapter(gson);
        }
    }

    public static TypeAdapter<AlbumResultsDataModel> typeAdapter(Gson gson) {
        return new AutoValue_AlbumResultsDataModel.GsonTypeAdapter(gson);
    }
}
