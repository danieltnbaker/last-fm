package com.danieltnbaker.lastfm.net;


import com.danieltnbaker.lastfm.net.model.AlbumResultsDataModel;
import com.danieltnbaker.lastfm.net.model.ArtistResultsDataModel;
import com.danieltnbaker.lastfm.net.model.TrackResultsDataModel;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface LastFmService {

    @GET("?method=album.search")
    Observable<AlbumResultsDataModel> performAlbumSearch(@QueryMap Map<String, Object> queryMap);

    @GET("?method=artist.search")
    Observable<ArtistResultsDataModel> performArtistSearch(@QueryMap Map<String, Object> queryMap);

    @GET("?method=track.search")
    Observable<TrackResultsDataModel> performTrackSearch(@QueryMap Map<String, Object> queryMap);
}
