package com.danieltnbaker.lastfm.net.model;


import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

@AutoValue
public abstract class ArtistDataModel {

    public abstract String getName();

    public abstract String getListeners();

    public abstract String getUrl();

    public abstract List<Image> getImage();

    public abstract String getStreamable();

    public abstract String getMbid();

    public static TypeAdapter<ArtistDataModel> typeAdapter(Gson gson) {
        return new AutoValue_ArtistDataModel.GsonTypeAdapter(gson);
    }
}
