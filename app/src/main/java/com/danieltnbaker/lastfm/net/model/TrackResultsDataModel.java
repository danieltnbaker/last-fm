package com.danieltnbaker.lastfm.net.model;


import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@AutoValue
public abstract class TrackResultsDataModel {

    public abstract TrackResultsDataModel.TrackResult getResults();

    @AutoValue
    public static abstract class TrackResult extends Result {

        @SerializedName("trackmatches")
        public abstract TrackResultsDataModel.TrackResult.TrackMatches getTrackMatches();

        @AutoValue
        public static abstract class TrackMatches {

            public abstract List<TrackDataModel> getTrack();

            public static TypeAdapter<TrackResultsDataModel.TrackResult.TrackMatches> typeAdapter(Gson gson) {
                return new AutoValue_TrackResultsDataModel_TrackResult_TrackMatches.GsonTypeAdapter(gson);
            }
        }

        public static TypeAdapter<TrackResultsDataModel.TrackResult> typeAdapter(Gson gson) {
            return new AutoValue_TrackResultsDataModel_TrackResult.GsonTypeAdapter(gson);
        }
    }

    public static TypeAdapter<TrackResultsDataModel> typeAdapter(Gson gson) {
        return new AutoValue_TrackResultsDataModel.GsonTypeAdapter(gson);
    }
}
