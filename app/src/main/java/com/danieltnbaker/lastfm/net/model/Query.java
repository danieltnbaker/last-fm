package com.danieltnbaker.lastfm.net.model;

import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class Query {

    @SerializedName("#text")
    public abstract String getText();

    public abstract String getRole();

    public abstract @Nullable String getSearchTerms();

    public abstract String getStartPage();

    public static TypeAdapter<Query> typeAdapter(Gson gson) {
        return new AutoValue_Query.GsonTypeAdapter(gson);
    }

}