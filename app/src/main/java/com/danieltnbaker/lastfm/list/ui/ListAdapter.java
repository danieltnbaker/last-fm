package com.danieltnbaker.lastfm.list.ui;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.danieltnbaker.lastfm.R;
import com.danieltnbaker.lastfm.model.Album;
import com.danieltnbaker.lastfm.model.Artist;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.model.Track;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private final OnItemClickListener mListener;

    interface OnItemClickListener {
        void onItemClicked(LastFmObject lastFmObject);
    }

    private List<LastFmObject> mData;

    ListAdapter(List<LastFmObject> data, OnItemClickListener listener) {
        mData = data;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.simple_album_list_item, parent, false);
        return new ViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    void addData(List<LastFmObject> data) {
        int prev = getItemCount();
        mData.addAll(prev, data);
        notifyItemRangeChanged(prev, mData.size());
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private final OnItemClickListener mListener;

        @BindView(R.id.thumbnail) ImageView thumbnail;
        @BindView(R.id.text1) TextView text1;
        @BindView(R.id.text2) TextView text2;
        @BindView(R.id.text3) TextView text3;

        ViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mListener = listener;
        }

        void bind(LastFmObject lastFmObject) {
            String thumbnailUrl = lastFmObject.getThumbnail();
            if (!TextUtils.isEmpty(thumbnailUrl)) {
                Picasso.with(itemView.getContext())
                        .load(thumbnailUrl)
                        .placeholder(R.drawable.ic_cd_cover)
                        .into(thumbnail);
            }
            if (lastFmObject instanceof Album) bindAlbumFields(((Album) lastFmObject));
            else if (lastFmObject instanceof Artist) bindArtistFields(((Artist) lastFmObject));
            else if (lastFmObject instanceof Track) bindTrackFields(((Track) lastFmObject));
            itemView.setOnClickListener(x -> mListener.onItemClicked(lastFmObject));
        }

        private void bindAlbumFields(Album album) {
            text1.setText(album.getName());
            text2.setText(album.getArtist());
            text2.setVisibility(View.VISIBLE);
            text3.setVisibility(View.GONE);
        }

        private void bindArtistFields(Artist artist) {
            text1.setText(artist.getName());
            text2.setVisibility(View.GONE);
            text3.setText(artist.getListeners());
        }

        private void bindTrackFields(Track track) {
            text1.setText(track.getName());
            text2.setText(track.getArtist());
            text2.setVisibility(View.VISIBLE);
            text3.setText(track.getListeners());
            text3.setVisibility(View.VISIBLE);
        }
    }
}
