package com.danieltnbaker.lastfm.list.di;

import com.danieltnbaker.lastfm.list.mvp.ListMvp;
import com.danieltnbaker.lastfm.list.mvp.ListPresenter;
import com.danieltnbaker.lastfm.net.LastFmRepo;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


@Module
public class ListModule {

    @Provides @Singleton
    ListMvp.Presenter provideListPresenter(LastFmRepo repo) {
        return new ListPresenter(repo, Schedulers.io(), AndroidSchedulers.mainThread());
    }
}
