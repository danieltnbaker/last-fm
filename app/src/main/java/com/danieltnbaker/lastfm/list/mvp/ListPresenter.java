package com.danieltnbaker.lastfm.list.mvp;

import android.support.annotation.VisibleForTesting;

import com.danieltnbaker.lastfm.model.Album;
import com.danieltnbaker.lastfm.model.Artist;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.model.Track;
import com.danieltnbaker.lastfm.net.LastFmRepo;
import com.danieltnbaker.lastfm.search.ui.SearchActivity;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;

import static com.danieltnbaker.lastfm.search.ui.SearchActivity.ALBUM;
import static com.danieltnbaker.lastfm.search.ui.SearchActivity.ARTIST;
import static com.danieltnbaker.lastfm.search.ui.SearchActivity.TRACK;


public class ListPresenter implements ListMvp.Presenter {

    private final LastFmRepo mRepo;
    private final Scheduler mSubscribeOnScheduler;
    private final Scheduler mObserveOnScheduler;

    private int mSearchType;
    private String mQuery;
    private int mPage;

    private ListMvp.View mView;
    private CompositeDisposable mDisposable;

    public ListPresenter(final LastFmRepo repo,
                         final Scheduler subscribeOnScheduler,
                         final Scheduler observeOnScheduler) {
        mRepo = repo;
        mSubscribeOnScheduler = subscribeOnScheduler;
        mObserveOnScheduler = observeOnScheduler;
    }

    @Override
    public void setView(ListMvp.View view) {
        mView = view;
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewReady(final @SearchActivity.SearchType int searchType,
                            final String query) {
        mPage = 1;
        getResultsFromService(mSearchType = searchType, mQuery = query);
    }

    @Override
    public void onEndOfPageReached() {
        mPage++;
        getResultsFromService(mSearchType, mQuery);
    }

    @Override
    public void releaseView() {
        if (mDisposable != null && !mDisposable.isDisposed()) mDisposable.dispose();
        mView = null;
    }

    @Override
    public void onItemClicked(LastFmObject lastFmObject) {
        mView.loadDetailsPage(lastFmObject);
    }

    private void getResultsFromService(final @SearchActivity.SearchType int mSearchType,
                                       final String query) {
        mView.showProgress();
        switch (mSearchType) {
            case TRACK:
                getTrackWithQuery(query);
                break;
            case ARTIST:
                getArtistWithQuery(query);
                break;
            case ALBUM:
                getAlbumsWithQuery(query);
                break;
        }
    }

    private void getAlbumsWithQuery(String query) {
        mDisposable.add(mRepo.performAlbumSearch(query, mPage)
                .subscribeOn(mSubscribeOnScheduler)
                .observeOn(mObserveOnScheduler)
                .flatMapIterable(x -> x.getResults().getAlbumMatches().getAlbum())
                .map(Album::create)
                .cast(LastFmObject.class)
                .toList()
                .subscribe(x -> {
                    mView.loadResults(x);
                    mView.hideProgress();
                }, this::onError));
    }

    private void getArtistWithQuery(String query) {
        mDisposable.add(mRepo.performArtistSearch(query, mPage)
                .subscribeOn(mSubscribeOnScheduler)
                .observeOn(mObserveOnScheduler)
                .flatMapIterable(x -> x.getResults().getArtistMatches().getArtist())
                .map(Artist::create)
                .cast(LastFmObject.class)
                .toList()
                .subscribe(x -> {
                    mView.loadResults(x);
                    mView.hideProgress();
                }, this::onError));
    }

    private void getTrackWithQuery(String query) {
        mDisposable.add(mRepo.performTrackSearch(query, mPage)
                .subscribeOn(mSubscribeOnScheduler)
                .observeOn(mObserveOnScheduler)
                .flatMapIterable(x -> x.getResults().getTrackMatches().getTrack())
                .map(Track::create)
                .cast(LastFmObject.class)
                .toList()
                .subscribe(x -> {
                    mView.loadResults(x);
                    mView.hideProgress();
                }, this::onError));
    }

    @SuppressWarnings("unused")
    private void onError(Throwable t) {
        mView.hideProgress();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    public void setValuesForTest(final @SearchActivity.SearchType int searchType,
                                 final String query) {
        mSearchType = searchType;
        mQuery = query;
    }
}
