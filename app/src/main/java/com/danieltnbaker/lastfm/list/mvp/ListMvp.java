package com.danieltnbaker.lastfm.list.mvp;

import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.search.ui.SearchActivity;

import java.util.List;

public interface ListMvp {

    interface Presenter {

        void setView(final ListMvp.View view);
        void onViewReady(@SearchActivity.SearchType int mSearchType, final String query);
        void onEndOfPageReached();
        void releaseView();
        void onItemClicked(LastFmObject lastFmObject);
    }

    interface View {
        void loadResults(List<LastFmObject> results);
        void loadDetailsPage(LastFmObject lastFmObject);
        void showProgress();
        void hideProgress();
    }
}
