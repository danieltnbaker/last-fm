package com.danieltnbaker.lastfm.list.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.danieltnbaker.lastfm.R;
import com.danieltnbaker.lastfm.detail.ui.DetailActivity;
import com.danieltnbaker.lastfm.di.DI;
import com.danieltnbaker.lastfm.list.mvp.ListMvp;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.search.ui.SearchActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListActivity extends AppCompatActivity implements ListMvp.View, ListAdapter.OnItemClickListener {

    private static final String EXTRA_SEARCH_TYPE = "EXTRA_SEARCH_TYPE";
    private static final String EXTRA_SEARCH_QUERY = "EXTRA_SEARCH_QUERY";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject ListMvp.Presenter mPresenter;
    private ListAdapter mAdapter;
    private AutoLoadOnScrollListener mScrollListener;

    public static Intent newIntent(final Context context,
                                   final @SearchActivity.SearchType int searchType,
                                   final String query) {
        Intent intent = new Intent(context, ListActivity.class);
        intent.putExtra(EXTRA_SEARCH_TYPE, searchType);
        intent.putExtra(EXTRA_SEARCH_QUERY, query);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        DI.getAppComponent().inject(this);
        mPresenter.setView(this);
        processIntent();
    }

    @Override
    protected void onDestroy() {
        mScrollListener.stopListening();
        mPresenter.releaseView();
        super.onDestroy();
    }

    @Override
    public void loadResults(List<LastFmObject> results) {
        if (mAdapter == null) {
            mAdapter = new ListAdapter(results, this);
            attachAdapterToRecyclerView();
        } else {
            mAdapter.addData(results);
        }
    }

    @Override
    public void loadDetailsPage(LastFmObject lastFmObject) {
        startActivity(DetailActivity.newIntent(this, lastFmObject));
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClicked(LastFmObject lastFmObject) {
        mPresenter.onItemClicked(lastFmObject);
    }

    private void attachAdapterToRecyclerView() {
        recyclerView.setAdapter(mAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mScrollListener = new AutoLoadOnScrollListener(mPresenter::onEndOfPageReached);
        recyclerView.addOnScrollListener(mScrollListener);
    }

    private void processIntent() {
        Intent intent = getIntent();
        int searchType = intent.getIntExtra(EXTRA_SEARCH_TYPE, -1);
        String searchQuery = intent.getStringExtra(EXTRA_SEARCH_QUERY);
        mPresenter.onViewReady(searchType, searchQuery);
    }
}
