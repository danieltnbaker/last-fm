package com.danieltnbaker.lastfm.di;

import com.danieltnbaker.lastfm.detail.di.DetailsModule;
import com.danieltnbaker.lastfm.detail.ui.DetailActivity;
import com.danieltnbaker.lastfm.list.di.ListModule;
import com.danieltnbaker.lastfm.list.ui.ListActivity;
import com.danieltnbaker.lastfm.search.di.SearchModule;
import com.danieltnbaker.lastfm.search.ui.SearchActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        SearchModule.class,
        ListModule.class,
        DetailsModule.class
})
public interface AppComponent {

    void inject(SearchActivity searchActivity);

    void inject(ListActivity listActivity);

    void inject(DetailActivity detailActivity);
}
