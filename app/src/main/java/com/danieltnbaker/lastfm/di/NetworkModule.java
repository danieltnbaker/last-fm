package com.danieltnbaker.lastfm.di;

import android.content.Context;

import com.danieltnbaker.lastfm.R;
import com.danieltnbaker.lastfm.net.AutoValueGsonFactory;
import com.danieltnbaker.lastfm.net.LastFmRepo;
import com.danieltnbaker.lastfm.net.LastFmService;
import com.google.gson.GsonBuilder;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    Cache provideOkHttpCache(Context context) {
        File httpCacheDirectory = new File(context.getCacheDir(), "okhttp_responses");
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        return new Cache(httpCacheDirectory, cacheSize);
    }

    @Provides
    @Singleton
    Interceptor provideNetworkInterceptor() {
        return chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();
            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter("api_key", "88933ec8bc2c46c1c53142871da0da7f")
                    .addQueryParameter("format", "json")
                    .build();
            Request.Builder requestBuilder = original.newBuilder().url(url);
            Request request = requestBuilder.build();
            return chain.proceed(request);
        };
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache, Interceptor interceptor) {
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(interceptor)
                .build();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create(
                new GsonBuilder().registerTypeAdapterFactory(AutoValueGsonFactory.create())
                        .create());
    }

    @Provides
    @Singleton
    LastFmService providesLastFmService(OkHttpClient okHttpClient,
                                        GsonConverterFactory converterFactory,
                                        Context context) {
        return new Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(LastFmService.class);
    }

    @Provides
    @Singleton
    LastFmRepo providesMenuRepo(final LastFmService service) {
        return new LastFmRepo(service);
    }
}
