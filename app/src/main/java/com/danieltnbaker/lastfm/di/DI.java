package com.danieltnbaker.lastfm.di;


public final class DI {

    private static AppComponent mAppComponent;

    public static void setAppComponent(AppComponent appComponent) {
        mAppComponent = appComponent;
    }

    public static AppComponent getAppComponent() {
        return mAppComponent;
    }

    private DI() {} // prevent creating instances
}
