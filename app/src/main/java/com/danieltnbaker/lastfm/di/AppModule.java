package com.danieltnbaker.lastfm.di;

import android.content.Context;

import com.danieltnbaker.lastfm.LastFmApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext() {
        return LastFmApplication.get();
    }
}
