package com.danieltnbaker.lastfm.detail.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.danieltnbaker.lastfm.R;
import com.danieltnbaker.lastfm.detail.mvp.DetailMvp;
import com.danieltnbaker.lastfm.di.DI;
import com.danieltnbaker.lastfm.model.Album;
import com.danieltnbaker.lastfm.model.Artist;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.model.Track;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailActivity extends AppCompatActivity implements DetailMvp.View {

    private static final String EXTRA_LAST_FM_OBJECT = "extra_last_fm_object";

    public static Intent newIntent(Context context, LastFmObject lastFmObject) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_LAST_FM_OBJECT, lastFmObject);
        return intent;
    }

    @BindView(R.id.cover_image) ImageView imageView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.text1) TextView textView;
    @BindView(R.id.text2) TextView textViewTwo;

    @Inject DetailMvp.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        DI.getAppComponent().inject(this);
        mPresenter.setView(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LastFmObject object = getIntent().getParcelableExtra(EXTRA_LAST_FM_OBJECT);
        mPresenter.onViewReady(object);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        mPresenter.releaseView();
        super.onDestroy();
    }

    @Override
    public void loadBasicInformation(LastFmObject lastFmObject) {
        Picasso.with(this).load(lastFmObject.getCover()).placeholder(R.drawable.ic_cd_cover).into(imageView);
    }

    @Override
    public void loadAlbumDetails(Album album) {
        setTitle(album.getName());
        textView.setText(album.getArtist());
        textViewTwo.setVisibility(View.GONE);
    }

    @Override
    public void loadArtistDetails(Artist artist) {
        setTitle(artist.getName());
        textView.setText(artist.getListeners());
        textViewTwo.setVisibility(View.GONE);
    }

    @Override
    public void loadTrackDetails(Track track) {
        setTitle(track.getName());
        textView.setText(track.getArtist());
        textViewTwo.setText(track.getListeners());
    }
}
