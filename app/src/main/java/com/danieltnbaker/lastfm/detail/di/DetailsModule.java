package com.danieltnbaker.lastfm.detail.di;

import com.danieltnbaker.lastfm.detail.mvp.DetailMvp;
import com.danieltnbaker.lastfm.detail.mvp.DetailsPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailsModule {

    @Provides @Singleton
    public DetailMvp.Presenter provideDetailsPresenter() {
        return new DetailsPresenter();
    }
}
