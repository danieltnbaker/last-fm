package com.danieltnbaker.lastfm.detail.mvp;


import com.danieltnbaker.lastfm.model.Album;
import com.danieltnbaker.lastfm.model.Artist;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.model.Track;

public interface DetailMvp {

    interface Presenter {
        void setView(View view);

        void onViewReady(LastFmObject lastFmObject);

        void releaseView();
    }

    interface View {
        void loadBasicInformation(LastFmObject lastFmObject);

        void loadAlbumDetails(Album album);

        void loadArtistDetails(Artist artist);

        void loadTrackDetails(Track track);
    }
}
