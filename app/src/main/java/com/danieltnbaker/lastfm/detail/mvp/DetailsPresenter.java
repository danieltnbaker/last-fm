package com.danieltnbaker.lastfm.detail.mvp;

import com.danieltnbaker.lastfm.model.Album;
import com.danieltnbaker.lastfm.model.Artist;
import com.danieltnbaker.lastfm.model.LastFmObject;
import com.danieltnbaker.lastfm.model.Track;


public class DetailsPresenter implements DetailMvp.Presenter {

    private DetailMvp.View mView;

    @Override
    public void setView(DetailMvp.View view) {
        mView = view;
    }

    @Override
    public void onViewReady(LastFmObject lastFmObject) {
        mView.loadBasicInformation(lastFmObject);
        if (lastFmObject instanceof Album) mView.loadAlbumDetails(((Album) lastFmObject));
        else if (lastFmObject instanceof Artist) mView.loadArtistDetails(((Artist) lastFmObject));
        else if (lastFmObject instanceof Track) mView.loadTrackDetails(((Track) lastFmObject));
    }

    @Override
    public void releaseView() {
        mView = null;
    }
}
