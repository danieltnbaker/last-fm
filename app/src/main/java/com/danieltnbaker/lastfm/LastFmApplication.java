package com.danieltnbaker.lastfm;

import android.app.Application;

import com.danieltnbaker.lastfm.di.DI;
import com.danieltnbaker.lastfm.di.DaggerAppComponent;


public class LastFmApplication extends Application {

    private static LastFmApplication sSelf;

    public LastFmApplication() {
        sSelf = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DI.setAppComponent(DaggerAppComponent.builder().build());
    }

    public static LastFmApplication get() {
        return sSelf;
    }

}
