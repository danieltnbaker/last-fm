package com.danieltnbaker.lastfm.model;

import com.danieltnbaker.lastfm.net.model.ArtistDataModel;
import com.danieltnbaker.lastfm.net.model.Image;
import com.danieltnbaker.lastfm.utils.LastFmUtils;
import com.google.auto.value.AutoValue;

import java.util.List;


@AutoValue
public abstract class Artist extends LastFmObject {

    public abstract String getListeners();

    public static Artist create(ArtistDataModel artistDataModel) {
        List<Image> images = artistDataModel.getImage();
        String thumbnail = getImageForSize(images, SIZE_MEDIUM);
        String cover = getImageForSize(images, SIZE_EXTRA_LARGE);
        String listeners = LastFmUtils.formatListeners(artistDataModel.getListeners());
        return create(artistDataModel.getName(), thumbnail, cover, listeners);
    }

    public static Artist create(String newName, String newThumbnail, String newCover, String newListeners) {
        return new AutoValue_Artist(newName, newThumbnail, newCover, newListeners);
    }
}
