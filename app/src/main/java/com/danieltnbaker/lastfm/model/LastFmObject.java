package com.danieltnbaker.lastfm.model;


import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.danieltnbaker.lastfm.net.model.Image;

import java.util.List;

public abstract class LastFmObject implements Parcelable {

    static final String SIZE_EXTRA_LARGE = "extralarge";
    static final String SIZE_MEDIUM = "medium";

    public abstract String getName();
    public abstract @Nullable String getThumbnail();
    public abstract @Nullable String getCover();

    static String getImageForSize(List<Image> images, String size) {
        if (images == null) return null;
        for (int i = 0, imagesSize = images.size(); i < imagesSize; i++) {
            Image image = images.get(i);
            if (image.getSize().equalsIgnoreCase(size)) {
                return image.getText();
            }
        }
        return null;
    }
}
