package com.danieltnbaker.lastfm.model;

import com.danieltnbaker.lastfm.net.model.AlbumDataModel;
import com.danieltnbaker.lastfm.net.model.Image;
import com.google.auto.value.AutoValue;

import java.util.List;

@AutoValue
public abstract class Album extends LastFmObject {

    public abstract String getArtist();

    public static Album create(AlbumDataModel albumDataModel) {
        List<Image> images = albumDataModel.getImage();
        String thumbnail = getImageForSize(images, SIZE_MEDIUM);
        String cover = getImageForSize(images, SIZE_EXTRA_LARGE);
        return Album.create(albumDataModel.getName(), thumbnail, cover, albumDataModel.getArtist());
    }

    public static Album create(String newName, String newThumbnail, String newCover, String newArtist) {
        return new AutoValue_Album(newName, newThumbnail, newCover, newArtist);
    }

}
