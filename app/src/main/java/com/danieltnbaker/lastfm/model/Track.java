package com.danieltnbaker.lastfm.model;

import com.danieltnbaker.lastfm.net.model.Image;
import com.danieltnbaker.lastfm.net.model.TrackDataModel;
import com.danieltnbaker.lastfm.utils.LastFmUtils;
import com.google.auto.value.AutoValue;

import java.util.List;

@AutoValue
public abstract class Track extends LastFmObject {

    public abstract String getArtist();
    public abstract String getListeners();

    public static Track create(TrackDataModel trackDataModel) {
        List<Image> images = trackDataModel.getImage();
        String thumbnail = getImageForSize(images, SIZE_MEDIUM);
        String cover = getImageForSize(images, SIZE_EXTRA_LARGE);
        String listeners = LastFmUtils.formatListeners(trackDataModel.getListeners());
        return create(trackDataModel.getName(), thumbnail, cover, trackDataModel.getArtist(),
                listeners);
    }

    public static Track create(String newName, String newThumbnail, String newCover,
                               String newArtist, String newListeners) {
        return new AutoValue_Track(newName, newThumbnail, newCover, newArtist, newListeners);
    }

}